# Kafka Streams In Action - Build Your Own Streaming App

Apache Kafka wird in Projekten immer mehr und mehr als "Globales Nervensystem" eingesetzt um Services zu entkoppeln. Dabei entstehen dann oft Lösungen mittels des einfachen Producer-Consumer-Mechanismus. Dabei gäbe es auch oft weitere
Lösungsmöglichkeiten mittels der Kafa-Streaming-API.
In diesem Vortrag möchte ich zeigen wie man eine Kafka-Streaming-Applikation relativ einfach schreiben kann.

Jeder der Interesse daran hat, kann auch gerne während des Vortrages "mitprogrammieren". Dazu einfach dieses Repo auschecken.

## Struktur des Repos

### [docker/](docker/)

Hier liegen die docker-compose-Scripte um die benötigte Umgebung (Apache Kafka, Shop-Simulator-App und Monitoring-App) zu starten.
Diese Umgebung wird benötigt um dann das eigentliche Streaming durchführen zu können.

### [shop-streaming/](shop-streaming/)

Die eigentliche Streaming-App. Ein Grundgerüst als Mavenprojekt mit den benötigten Event-Klassen ist bereits vorhanden.


