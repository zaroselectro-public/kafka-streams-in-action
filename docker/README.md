# docker

Verschiedene docker-compose-Scripte zum Starten der Anwendung

| Datei                                                                  | Beschreibung                                                         |
|------------------------------------------------------------------------|----------------------------------------------------------------------|
| [docker-compose.yml](docker-compose.yml)                               | zum Starten von Apache Kafka, Monitoring-App und Shop-Simulation-App |
| [docker-compose-kafka.yml](docker-compose-kafka.yml)                   | zum Starten von Apache Kafka                                         |
| [docker-compose-monitoring.yml](docker-compose-monitoring.yml)         | zum Starten der Monitoring-App                                       |
| [docker-compose-shop-simulator.yml](docker-compose-shop-simulator.yml) | zum Starten der Shop-Simulation-App                                  |
