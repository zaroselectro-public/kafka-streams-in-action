# Streaming-App

Die eigentliche Streaming-App. Ein Grundgerüst als Mavenprojekt mit den benötigten Event-Klassen ist bereits vorhanden.

## Bauen der App

```
mvn clean install
```

## Starten der App

```
java -jar target/shop-streaming-0.0.1-SNAPSHOT-jar-with-dependencies.jar
```
