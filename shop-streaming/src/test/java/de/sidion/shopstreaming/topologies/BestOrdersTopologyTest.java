package de.sidion.shopstreaming.topologies;

import de.sidion.shopstreaming.events.OrderEvent;
import de.sidion.shopstreaming.serde.SerdeFactory;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Properties;

class BestOrdersTopologyTest {

    private TopologyTestDriver testDriver;
    private TestInputTopic<String, OrderEvent> inputTopicOrders;
    private TestOutputTopic<String, OrderEvent> outputTopic;


    @BeforeEach
    void setUp() {

        final Properties streamsConfiguration = new Properties();
        streamsConfiguration.put(StreamsConfig.APPLICATION_ID_CONFIG, "test01");
        streamsConfiguration.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "dummy:9092");


        final StreamsBuilder builder = new StreamsBuilder();

        final BestOrdersTopology bestOrdersTopology = new BestOrdersTopology();
        bestOrdersTopology.createTopology(builder);


        final Topology topology = builder.build();
        testDriver = new TopologyTestDriver(topology, streamsConfiguration);
        inputTopicOrders = testDriver
                .createInputTopic("orders", Serdes.String().serializer(), SerdeFactory.serdeForOrderEvent().serializer());

        outputTopic = testDriver
                .createOutputTopic("best-orders", Serdes.String().deserializer(), SerdeFactory.serdeForOrderEvent().deserializer());


    }

    @AfterEach
    void tearDown() {
        testDriver.close();
    }

    @Test
    void testBestOrdersTopology() {


    }
}
