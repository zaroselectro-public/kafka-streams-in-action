package de.sidion.shopstreaming.topologies;

import de.sidion.shopstreaming.events.AggregatedArticleEvent;
import de.sidion.shopstreaming.events.ArticleEvent;
import de.sidion.shopstreaming.events.OrderEvent;
import de.sidion.shopstreaming.serde.SerdeFactory;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Properties;

class BestArticlesTopologyTest {

    private TopologyTestDriver testDriver;
    private TestInputTopic<String, OrderEvent> inputTopicOrders;
    private TestInputTopic<String, ArticleEvent> inputTopicArticles;
    private TestOutputTopic<String, AggregatedArticleEvent> outputTopic;


    @BeforeEach
    void setUp() {

        final Properties streamsConfiguration = new Properties();
        streamsConfiguration.put(StreamsConfig.APPLICATION_ID_CONFIG, "test01");
        streamsConfiguration.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "dummy:9092");


        final StreamsBuilder builder = new StreamsBuilder();

        final BestArticlesTopology bestArticlesTopology = new BestArticlesTopology();
        bestArticlesTopology.createTopology(builder);


        final Topology topology = builder.build();
        testDriver = new TopologyTestDriver(topology, streamsConfiguration);
        inputTopicOrders = testDriver
                .createInputTopic("orders", Serdes.String().serializer(), SerdeFactory.serdeForOrderEvent().serializer());

        inputTopicArticles = testDriver
                .createInputTopic("articles", Serdes.String().serializer(), SerdeFactory.serdeForArticleEvent().serializer());

        outputTopic = testDriver
                .createOutputTopic("best-articles", Serdes.String().deserializer(), SerdeFactory.serdeForAggregatedArticleEvent().deserializer());


    }

    @AfterEach
    void tearDown() {
        testDriver.close();
    }

    @Test
    void testBestArticlesTopology() {

    }
}
