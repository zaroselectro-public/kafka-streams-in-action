package de.sidion.shopstreaming.topologies;

import de.sidion.shopstreaming.events.AggregatedCustomerEvent;
import de.sidion.shopstreaming.events.CustomerEvent;
import de.sidion.shopstreaming.events.OrderEvent;
import de.sidion.shopstreaming.serde.SerdeFactory;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Map;
import java.util.Properties;

class BestCustomersTopologyTest {

    private TopologyTestDriver testDriver;
    private TestInputTopic<String, OrderEvent> inputTopicOrders;
    private TestInputTopic<String, CustomerEvent> inputTopicArticles;
    private TestOutputTopic<String, AggregatedCustomerEvent> outputTopic;


    @BeforeEach
    void setUp() {

        final Properties streamsConfiguration = new Properties();
        streamsConfiguration.put(StreamsConfig.APPLICATION_ID_CONFIG, "test01");
        streamsConfiguration.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "dummy:9092");


        final StreamsBuilder builder = new StreamsBuilder();

        final BestCustomersTopology bestCustomersTopology = new BestCustomersTopology();
        bestCustomersTopology.createTopology(builder);


        final Topology topology = builder.build();
        testDriver = new TopologyTestDriver(topology, streamsConfiguration);
        inputTopicOrders = testDriver.createInputTopic("orders", Serdes.String().serializer(), SerdeFactory.serdeForOrderEvent().serializer());
        inputTopicArticles = testDriver.createInputTopic("customers", Serdes.String().serializer(), SerdeFactory.serdeForCustomerEvent().serializer());
        outputTopic = testDriver.createOutputTopic("best-customers", Serdes.String().deserializer(), SerdeFactory.serdeForAggregatedCustomerEvent().deserializer());


    }

    @AfterEach
    void tearDown() {
        testDriver.close();
    }

    @Test
    void testBestCustomersTopology() {

        final CustomerEvent customerEvent1 = CustomerEvent.builder().id("customerIdx01").email("foo1@bar.de").name("name01").consentNl(true).build();
        final CustomerEvent customerEvent2 = CustomerEvent.builder().id("customerIdx02").email("foo2@bar.de").name("name02").consentNl(false).build();

        final OrderEvent orderEvent1 = OrderEvent.builder().idx("orderIdx01").articleId("artIdx01").customerId("customerIdx01").amount(200).build();
        final OrderEvent orderEvent2 = OrderEvent.builder().idx("orderIdx02").articleId("artIdx02").customerId("customerIdx02").amount(100).build();
        final OrderEvent orderEvent3 = OrderEvent.builder().idx("orderIdx03").articleId("artIdx01").customerId("customerIdx01").amount(200).build();

        inputTopicArticles.pipeInput(customerEvent1.getId(), customerEvent1);
        inputTopicArticles.pipeInput(customerEvent2.getId(), customerEvent2);
        inputTopicOrders.pipeInput(orderEvent1.getIdx(), orderEvent1);
        inputTopicOrders.pipeInput(orderEvent2.getIdx(), orderEvent2);
        inputTopicOrders.pipeInput(orderEvent3.getIdx(), orderEvent3);

        final Map<String, AggregatedCustomerEvent> result = outputTopic.readKeyValuesToMap();
        Assertions.assertEquals(2, result.size());
        Assertions.assertEquals(400, result.get("customerIdx01").getAmount());

    }
}
