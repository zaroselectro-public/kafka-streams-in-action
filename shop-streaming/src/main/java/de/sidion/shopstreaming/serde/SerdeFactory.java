package de.sidion.shopstreaming.serde;

import de.sidion.shopstreaming.events.*;
import org.apache.kafka.common.serialization.Serde;

public class SerdeFactory {

    public static Serde<AggregatedCustomerEvent> serdeForAggregatedCustomerEvent() {
        final CustomJsonSerde<AggregatedCustomerEvent> orderEventCustomJsonSerde = new CustomJsonSerde<>(AggregatedCustomerEvent.class);
        return orderEventCustomJsonSerde.get();
    }

    public static Serde<AggregatedArticleEvent> serdeForAggregatedArticleEvent() {
        final CustomJsonSerde<AggregatedArticleEvent> orderEventCustomJsonSerde = new CustomJsonSerde<>(AggregatedArticleEvent.class);
        return orderEventCustomJsonSerde.get();
    }

    public static Serde<OrderEvent> serdeForOrderEvent() {
        final CustomJsonSerde<OrderEvent> orderEventCustomJsonSerde = new CustomJsonSerde<>(OrderEvent.class);
        return orderEventCustomJsonSerde.get();
    }

    public static Serde<ArticleEvent> serdeForArticleEvent() {
        final CustomJsonSerde<ArticleEvent> orderEventCustomJsonSerde = new CustomJsonSerde<>(ArticleEvent.class);
        return orderEventCustomJsonSerde.get();
    }

    public static Serde<CategoryEvent> serdeForCategoryEvent() {
        final CustomJsonSerde<CategoryEvent> orderEventCustomJsonSerde = new CustomJsonSerde<>(CategoryEvent.class);
        return orderEventCustomJsonSerde.get();
    }

    public static Serde<CustomerEvent> serdeForCustomerEvent() {
        final CustomJsonSerde<CustomerEvent> orderEventCustomJsonSerde = new CustomJsonSerde<>(CustomerEvent.class);
        return orderEventCustomJsonSerde.get();
    }


}
