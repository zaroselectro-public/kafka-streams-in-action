package de.sidion.shopstreaming.topologies;

import de.sidion.shopstreaming.events.AggregatedCustomerEvent;
import de.sidion.shopstreaming.events.CustomerEvent;
import de.sidion.shopstreaming.events.OrderEvent;
import de.sidion.shopstreaming.serde.SerdeFactory;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.kstream.*;

public class BestCustomersTopology {


    public void createTopology(final StreamsBuilder builder) {

        final Serde<String> stringSerde = Serdes.String();
        final Serde<CustomerEvent> customerEventSerde = SerdeFactory.serdeForCustomerEvent();
        final Serde<OrderEvent> orderEventSerde = SerdeFactory.serdeForOrderEvent();
        final Serde<AggregatedCustomerEvent> aggregatedCustomerEventSerde = SerdeFactory.serdeForAggregatedCustomerEvent();

        final KStream<String, OrderEvent> orders =
                builder.stream("orders", Consumed.with(stringSerde, orderEventSerde));

        final KTable<String, CustomerEvent> customers = builder.table("customers", Consumed.with(stringSerde, customerEventSerde));


        final KStream<String, OrderEvent> ordersWithNewKey = orders.selectKey((k, v) -> String.valueOf(v.getCustomerId()));


        final ValueJoiner<OrderEvent, CustomerEvent, OrderEvent> valueJoiner = new ValueJoiner<>() {
            @Override
            public OrderEvent apply(final OrderEvent orderEvent, final CustomerEvent articleEvent) {
                orderEvent.setCustomerEvent(articleEvent);
                return orderEvent;
            }
        };


        final KStream<String, OrderEvent> joinedOrderStream = ordersWithNewKey.join(customers, valueJoiner,
                Joined.with(stringSerde, orderEventSerde, customerEventSerde));


        final KGroupedStream<String, OrderEvent> groupedOrderStream = joinedOrderStream.groupByKey(Grouped.with(stringSerde, orderEventSerde));


        final KTable<String, AggregatedCustomerEvent> aggregatedCustomers
                = groupedOrderStream.aggregate(() -> new AggregatedCustomerEvent(0),
                (key, value, aggregate) -> {
                    aggregate.setAmount(aggregate.getAmount() + value.getAmount());
                    aggregate.setEmail(value.getCustomerEvent().getEmail());
                    aggregate.setCustomerId(value.getCustomerId());
                    return aggregate;
                },
                Materialized.with(stringSerde, aggregatedCustomerEventSerde));

        aggregatedCustomers.toStream().to("best-customers", Produced.with(stringSerde, aggregatedCustomerEventSerde));


    }
}
