package de.sidion.shopstreaming.events;

public class AggregatedArticleEvent {


    private Integer count;
    private String name;
    private String articleId;

    public AggregatedArticleEvent() {
    }

    public AggregatedArticleEvent(Integer count) {
        this.count = count;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArticleId() {
        return articleId;
    }

    public void setArticleId(String articleId) {
        this.articleId = articleId;
    }
}
