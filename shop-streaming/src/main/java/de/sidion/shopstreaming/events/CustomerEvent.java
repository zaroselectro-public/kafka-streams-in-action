package de.sidion.shopstreaming.events;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
public class CustomerEvent {

    private String id;
    private String name;
    private String email;
    private boolean consentNl;

}
