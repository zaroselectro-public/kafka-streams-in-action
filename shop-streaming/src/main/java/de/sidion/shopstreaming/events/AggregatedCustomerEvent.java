package de.sidion.shopstreaming.events;

public class AggregatedCustomerEvent {


    private Integer amount;
    private String email;
    private String customerId;

    public AggregatedCustomerEvent() {
    }

    public AggregatedCustomerEvent(Integer amount) {
        this.amount = amount;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String name) {
        this.email = name;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }
}
