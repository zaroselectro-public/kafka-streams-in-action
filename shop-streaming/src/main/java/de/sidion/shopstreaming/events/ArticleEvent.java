package de.sidion.shopstreaming.events;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
public class ArticleEvent {

    private String id;
    private String name;
    private Integer price;
    private String categoryId;
}
