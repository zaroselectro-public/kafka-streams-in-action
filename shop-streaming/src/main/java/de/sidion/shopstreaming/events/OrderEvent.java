package de.sidion.shopstreaming.events;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
public class OrderEvent {

    private String idx;
    private String customerId;
    private String articleId;
    private Integer amount;
    private Integer numberOfArticles;

    private ArticleEvent articleEvent;

    private CustomerEvent customerEvent;

}
